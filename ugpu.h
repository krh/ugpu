#ifndef _UGPU_H_
#define _UGPU_H_

#include <linux/types.h>
#include <asm/ioctl.h>

/*
 * What is the purpose of all this?
 *
 * - avoid rewriting so much of mm subsystem. Use memfd for allocation, which
 *   lets use use madvise and mmap directly, avoids create/destroy and buffer
 *   handles.
 *
 * - minimal synchronization/book-keeping on kernel side. Kernel doesn't maange
 *   cache domains, flushing or synchronize iommu mappings.
 *
 * - no bo_busy or wait bo calls, just a helper to wake userspace on interrupt,
 *   if a given value has been written to an address. poll on fd or maybe uring
 *   type of thing for blocking/waiting
 *
 * - dmabufs can be directly imported by mapping them to an iova, export via
 *   udmabuf (or maybe we don't want to depend on that, could have ioctl to
 *   export slice of memfd... which is what udmabuf does).
 *
 * - requires: UMA, per-context iommu, pipelined irq and 64 bit data write
 *
 * - dmabuf export: just use udmabuf?
 *
 * - dmabuf import: just allow dmabuf fds in ugpu_map??
 *
 * - cache coherency, cache management? clflush on x86 but what about ARM?
 *
 * - create/destroy hw context?
 */


#define UGPU_IOCTL_BASE 'G'

/* This is the generic get capability/param/property ioctl. Properties are
 * nameespaced like format modifers, with a generic space and a driver
 * space. Properties can be either simple u64 values or a sized blob, in which
 * case value is a userspace pointer and length is in/out.  doesn't write
 * anything if length is too small, returns bytes needed (that would've been
 * written).
 */

#define UGPU_IOCTL_QUERY	_IOWR(UGPU_IOCTL_BASE, 0, struct ugpu_query)

#define __UGPU_PROP_CODE(driver, value) ( ((__u64) (driver) << 56) | (value) )

#define UGPU_CORE_CODE			0

#define UGPU_PROP_INVALID		__UGPU_PROP_CODE(UGPU_CORE_CODE, 0)
#define UGPU_PROP_DRIVER_NAME		__UGPU_PROP_CODE(UGPU_CORE_CODE, 1)
#define UGPU_PROP_DRIVER_VERSION	__UGPU_PROP_CODE(UGPU_CORE_CODE, 2)

struct ugpu_query {
	__u64 property; /* property enum - same namespace structure as modifiers */
	__u64 value;  /* for immediate properties: out: value, for blob properties: in: user pointer */
	__u64 length; /* for blob properties: in: sizeof buffer, out: bytes written */
};

/* Map range from shm or dmabuf fd to io virtual address. The range does not
 * need to be mmapped to CPU virtual address.  This marks the range as
 * resident, which means it will be pinned while the gpu is busy.
 *
 * By allowing a dmabuf fd, we can directly use dmabufs, no need for special
 * import. Buffers can also be shared by sharing a memfd.
 *
 * Mapping after unmap is generally fast if the memory hasn't been evicted,
 * and map/unmap should be usable as a way to manage residency.
 */
#define UGPU_IOCTL_MAP		_IOW(UGPU_IOCTL_BASE, 1, struct ugpu_map)

#define UGPU_MAP_READ		0x01
#define UGPU_MAP_WRITE		0x02
#define UGPU_MAP_POPULATE	0x04 /* not sure this makes sense, we can
				      * populate in mmap if we map it,
				      * otherwise, submit will populate when
				      * we use the range*/
#define UGPU_MAP_NAME		0x08 /* name points to userspace debug name */

struct ugpu_map {
	__u64 iova;
	__u64 length;
	__u32 flags;
	__u32 fd;
	__u64 offset;

	__u64 name;
};

/* unmap range from io virtual address. This makes the io address range no
 * longer usable with the gpu, but doesn't immediately unbind the pages.
 * unmap is lazy and cheap: things only really get evicted under memory
 * pressure or if something else gets mapped over.  As such, map/unmap can be
 * used frequently to manage resident set, but it's also possible to just
 * leave everything resident.
 */
#define UGPU_IOCTL_UNMAP	_IOW(UGPU_IOCTL_BASE, 2, struct ugpu_unmap)

struct ugpu_unmap {
	__u64 iova;
	__u64 length;
};

/* Execute command buffer at iova, lenght bytes. userspace manages mapping and
 * unmapping buffers to iova and synchronization for those mappings.  No
 * flushing done by submit. No relocs, doesn't keep maps active.
 *
 * residency: all mapped regions are considered resident.
 *
 * xxx: not all gpus need length, what does it even mean for intel chained
 *   batch buffers?
 *
 * xxx: multiple iova's? for submitting multiple batches atomically... but
 *   that implies (like msm.ko) that other processes can mess up our state,
 *   which we may not want to allow.
 *
 * xxx: do we need WRITE flags for ranges that get written or does iommu track
 *   that with dirty bits?
 *
 * xxx: fence fds?
 *
 * xxx: length is needed for freedreno
 *
 * xxx: how does the kernel get and put pages, when it doesn't know which
 *   buffers are busy? track busy per context and put pages for all resident
 *   maps when no longer busy? or put in shrinker if region is idle (region
 *   last busy serial number older than most recent gpu serial).
 *
 */
#define UGPU_IOCTL_SUBMIT	_IOWR(UGPU_IOCTL_BASE, 3, struct ugpu_submit)

struct ugpu_submit {
  __u64 iova;
  __u64 length;
};

/* Request that interrupts generate event when the contents of iova is
 * value.
 *
 * xxx: Considered to use a regular futex, but need 64 bit value
 *   typically. Also only ever woken from GPU so don't need the futex_wake
 *   side of things.
 *
 * xxx: Tempting to just make this "wake up on interrupt" and leave the
 *   parsing to userspace, but some GPUs have only one interrupt and no way to
 *   determine what triggered the interrupt, so could cause a lot of false
 *   wakeups.
 *
 * xxx: Do we need to specify wakeup criteria/ops or is "wakeup when equal"
 *   enough?  intended use is for gpu to write back serial number from
 *   somewhere down the pipeline and wake up when we see that.
 *
 * xxx: do we need a 32 bit version?
 *
 * xxx: version that signals a dma-fence instead of waking up userspace?
 *   possible to use multiple WAKEs on same iova, different or same value,
 *   userspace event or signal dma-fence.
 */
#define UGPU_IOCTL_WAKE		_IOW(UGPU_IOCTL_BASE, 4, struct ugpu_wake)

struct ugpu_wake {
  __u64 iova; /* 64 bit alignment, must not span pages, may want to use kmap */
  __u64 value;
  __u64 userdata; /* send this back with the event */
};

/* Collects a wake request. non-blocking, use poll with POLLIN on fd to block.
 * Returns EWOULDBLOCK if is not ready to collect.
 */
#define UGPU_IOCTL_COLLECT	_IOR(UGPU_IOCTL_BASE, 5, struct ugpu_collect)

struct ugpu_collect {
  __u64 iova; /* 64 bit alignment, must not span pages */
  __u64 userdata; /* from wake ioctl */
};

#endif

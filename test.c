#include <unistd.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/memfd.h>
#include <linux/futex.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include "ugpu.h"
#include "ugpu_freedreno.h"

static inline void
fail_if(bool cond, const char *msg)
{
	if (cond) {
		fprintf(stderr, "%s failed (errno %m)\n", msg);
		exit(EXIT_FAILURE);
	}
}

static inline int
memfd_create(const char *name, unsigned int flags)
{
	return syscall(SYS_memfd_create, name, flags);
}

static inline uint64_t
ptr2u64(void *p)
{
	return (uint64_t) (long) p;
}

static void
build_batch(void *pool)
{
	/* xxx: command stream building */
}

int main(int argc, char *argv[])
{
	const uint32_t pool_size = 2 << 20;
	int ret;

	/* cuse? */
	int device = open("/dev/null", O_RDWR | O_CLOEXEC);
	fail_if(device == -1, "open");

	char buffer[1024];
	struct ugpu_query q0 = {
		.property = UGPU_PROP_DRIVER_NAME,
		.value = ptr2u64(buffer),
		.length = sizeof(buffer)
	};
	ret = ioctl(device, UGPU_IOCTL_QUERY, &q0);
	fail_if(ret == -1, "UGPU_IOCTL_QUERY");
	printf("driver name: %s\n", buffer);

	struct ugpu_query q1 = {
		.property = FREEDRENO_PROP_GMEM_SIZE,
	};
	ret = ioctl(device, UGPU_IOCTL_QUERY, &q1);
	fail_if(ret == -1, "UGPU_IOCTL_QUERY");
	printf("freedreno gmem: %d\n", (int) q1.value);

	void *pool;
	int mfd = memfd_create("main pool", MFD_CLOEXEC);
	fail_if(mfd == -1, "memfd_create");

	ret = ftruncate(mfd, pool_size);
	fail_if(ret == -1, "ftruncate");

	pool = mmap(NULL, pool_size, PROT_READ | PROT_WRITE,
		    MAP_SHARED | MAP_POPULATE, mfd, 0);
	fail_if (pool == MAP_FAILED, "mmap");

	struct ugpu_map m = {
		.iova = 0x1000, /* iova address space managed by userspace */
		.length = 0x2000,
		.flags = UGPU_MAP_READ | UGPU_MAP_WRITE, /* can map read-only */
		.fd = mfd,
		.offset = 0 /* doesn't have to be same as iova address */
	};
	ret = ioctl(device, UGPU_IOCTL_MAP, &m);
	fail_if(ret == -1, "UGPU_IOCTL_MAP");

	/* build a batch buffer. start address is 0x200 bytes in (iova 0x1200) and it
	 * writes uint64_t at offset 0 (iova 0x1000).
	 */
	build_batch(pool);

	/* register wake event for iova 0x1000 (in above map) */
	struct ugpu_wake w = {
		.iova = 0x1000,
		.value = 0xcafebabe,
		.userdata = 0
	};
	ret = ioctl(device, UGPU_IOCTL_WAKE, &w);
	fail_if(ret == -1, "UGPU_IOCTL_WAKE");

	/* write batch buffer to pool */
	struct ugpu_submit s = {
		.iova = 0x1200 /* this is where cmd batch starts */
	};
	ret = ioctl(device, UGPU_IOCTL_SUBMIT, &s);
	fail_if(ret == -1, "UGPU_IOCTL_SUBMIT");

	/* wait for ugpu event... */
	uint64_t *seq = pool;

	/* xxx: memory barriers, cache invalidation here. */
	while (*seq != 0xcafebabe) {
		struct pollfd pfd[1];
		pfd[0].fd = device;
		pfd[0].events = POLLIN;
		ret = poll(pfd, 1, 0);
		fail_if(ret == -1, "poll");

		struct ugpu_collect c;
		ret = ioctl(device, UGPU_IOCTL_COLLECT, &c);
		fail_if(ret == -1, "UGPU_IOCTL_COLLECT");
	}

	/* We can call regular Linux madvise... POSIX_MADV_DONTNEED don't have
	 * the "discard contents" semantics though. No way to DONTNEED memory
	 * without mmapping it, since madvise works on CPU addresses... Or
	 * maybe if you ugpu_unmap and it's not mmap'ed, that's same as
	 * DONTNEED? Doesn't seem right either, since then there's no way to
	 * retain a buffer when it's not in the resident set or mmaped.. */
	madvise(pool, 8 * 1024, MADV_DONTNEED);

	/* or we could unmap it at this point */
	/* write batch buffer to pool */
	struct ugpu_unmap u = {
		.iova = 0x1000,
		.length = 0x2000
	};
	ret = ioctl(device, UGPU_IOCTL_UNMAP, &u);
	fail_if(ret == -1, "UGPU_IOCTL_UNMAP");

	printf("ugpu\n");
}
